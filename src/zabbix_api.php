<?php

namespace Drupal\zabbixfrontend;
use Drupal\Core\Logger;
use Drupal\Core\Form\ConfigFormBase;

/**
 * @brief   Concrete class for the Zabbix API.
 */

class zabbix_api extends ZabbixApiAbstract
{
	
public $client;
public $tempstore;
private $user;
private $password;
private $printCommunication = true;

    public function __construct()
    {
		$config=\Drupal::config('zabbixfrontend.settings');
		$this->client=\Drupal::httpClient();
		$this->tempstore = \Drupal::service('user.private_tempstore')->get('zabbixfrontend');
		$apiUrl=$config->get('zabbix_api_url');
		$this->user=$config->get('zabbix_api_user');
		$this->password=$config->get('zabbix_api_pass');
		$this->printCommunication=$config->get('zabbix_api_debug');
    //    $this->printCommunication=true;
        if($apiUrl)
            $this->setApiUrl($apiUrl);
        if($this->user && $this->password)
            $this->userLogin(array('user' => $this->user, 'password' => $this->password));
    }

	    public function request($method, $params=NULL, $resultArrayKey='', $auth=TRUE)
    {

        // sanity check and conversion for params array
        if(!$params)                $params = array();
        elseif(!is_array($params))  $params = array($params);

        // generate ID
        $this->id = number_format(microtime(true), 4, '', '');

        // build request array
        $this->request = array(
            'jsonrpc' => '2.0',
            'method'  => $method,
            'params'  => $params,
            'id'      => $this->id
        );

        // add auth token if required
        if ($auth)
            $this->request['auth'] = ($this->authToken ? $this->authToken : NULL);

        // encode request array
        $this->requestEncoded = json_encode($this->request);

        // debug logging
        if($this->printCommunication)
			debug('Request: '.$this->requestEncoded, 'zabbixfrontend');

        // get response
		$this->response = $this->client->get($this->getApiUrl(),array(
				'headers' => array('Content-Type' => 'application/json'),
				'auth' => [$this->user,$this->password],
				'body' => $this->requestEncoded)
			);

         // response verification
        if($this->response === FALSE)
            throw new Exception('Could not read data from "'.$this->getApiUrl().'"');

        // decode response
        $this->responseDecoded = json_decode($this->response->getBody(),true);
        // debug logging
        if($this->printCommunication)
            debug('Response: '.print_r($this->responseDecoded,true), 'zabbixfrontend');
        return $this->responseDecoded['result'];
    }


}



?>