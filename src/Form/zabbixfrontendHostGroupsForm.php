<?php

namespace Drupal\zabbixfrontend\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

class zabbixfrontendHostGroupsForm extends FormBase {
  /** 
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'zabbix_frontend_hostgroup_list';
  }

  /** 
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'zabbix_frontend.settings',
    ];
  }

  /** 
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
		$options=array();
		$header=array('name' => t('Group Name'),
					  'num_hosts' => t('Number of hosts'),
					'triggers' => t('Triggers'));
		$hostgroups=$form_state->get('hostgroups');
		foreach ($hostgroups as $group) {
			$options[$group['groupid']]=array('name'=>$group['name'],
					'num_hosts' => $group['numhosts'],
					'triggers' => '');
			
		}
		$form['table'] = array(
			'#type' => 'tableselect',
			'#header' => $header,
			'#options' => $options,
			'#empty' => print_r($hostgroups,true),
		  );
		$form['submit_new'] = array(
			'#type' => 'submit',
			'#value' => t('Add group'),
			'#disabled' => true,
		  );
		$form['submit_view'] = array(
			'#type' => 'submit',
			'#value' => t('View hosts'),
			'#disabled' => true,
		  );
 
    return $form;
  }

  /** 
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
  }
}


?>