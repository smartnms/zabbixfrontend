<?php

namespace Drupal\zabbixfrontend\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

class zabbixfrontendHostsForm extends FormBase {
  /** 
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'zabbix_frontend_hostgroup_list';
  }

  /** 
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'zabbix_frontend.settings',
    ];
  }

  /** 
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
		$options=array();
		$header=array('name' => t('Group Name'),
					  'desc' => t('Description'),				
					  'agente' => t('Agent Status'),		
					  'interfaces' => t('Interfaces'),
					'triggers' => t('Triggers'));
		$hosts=$form_state->get('hosts');
		foreach ($hosts as $host) {
			$options[$host['hostid']]=array('name'=>$host['host'],
					'desc' => $host['description'],			
					'agente' => $host['available'],
					'interfaces' => '',
					'triggers' => '');
			
		}
		$form['table'] = array(
			'#type' => 'tableselect',
			'#header' => $header,
			'#options' => $options,
			'#empty' => print_r($hosts,true),
		  );
		$form['submit_new'] = array(
			'#type' => 'submit',
			'#value' => t('Add host'),
			'#disabled' => true,
		  );
    return $form;
  }

  /** 
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
  }
}


?>