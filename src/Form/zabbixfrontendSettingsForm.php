<?php

namespace Drupal\zabbixfrontend\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure zabbixfrontend settings for this site.
 */
class zabbixfrontendSettingsForm extends ConfigFormBase {
  /** 
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'zabbix_frontend_admin_settings';
  }

  /** 
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'zabbixfrontend.settings',
    ];
  }

  /** 
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('zabbixfrontend.settings');

    $form['zabbix_frontend_apiurl'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('URL for API connection'),
      '#default_value' => $config->get('zabbix_api_url'),
    );  

    $form['zabbix_frontend_user'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('User for API connection'),
      '#default_value' => $config->get('zabbix_api_user'),
    );  

	$form['zabbix_frontend_pass'] = array(
      '#type' => 'password',
      '#title' => $this->t('Password for the User in API connection'),
      '#default_value' => $config->get('zabbix_api_pass'),
    );  
	$form['zabbix_frontend_debug'] = array(
      '#type' => 'checkbox',
      '#title' => $this->t('Enable debug mode'),
      '#default_value' => $config->get('zabbix_api_debug'),
    );  
    return parent::buildForm($form, $form_state);
  }

  /** 
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Retrieve the configuration
    $this->config('zabbixfrontend.settings')
      // Set the submitted configuration setting
      ->set('zabbix_api_url', $form_state->getValue('zabbix_frontend_apiurl'))
      // You can set multiple configurations at once by making
      // multiple calls to set()
      ->set('zabbix_api_user', $form_state->getValue('zabbix_frontend_user'))
	  ->set('zabbix_api_pass', $form_state->getValue('zabbix_frontend_pass'))
      ->set('zabbix_api_debug', $form_state->getValue('zabbix_frontend_debug'))
      ->save();

    parent::submitForm($form, $form_state);
  }
}


?>