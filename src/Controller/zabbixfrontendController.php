<?php
/**
 * @file
 * Contains \Drupal\example\Controller\ExampleController.
 */
namespace Drupal\zabbixfrontend\Controller;
use Drupal\Core\Controller\ControllerBase;
use Drupal\zabbixfrontend\zabbix_api;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Form\FormState;
use Drupal\Core\Url;
use Drupal\Core\Link;

class zabbixfrontendController {
	
	public function startAction() {
		$api=new zabbix_api();
		$form_state = new FormState();
		$hostgroups=$api->hostgroupGet(array(
        'output' => 'extend',
        'search' => array()
		));
		foreach($hostgroups as $key => $group)
			{
			$count=count($api->hostGet(array(
			'output' => 'shorten',
			'groupids'=>$group['groupid'],
			)));
			if(!is_int($count))
				$count=0;
			$hostgroups[$key]['numhosts']=$count;
			}
		$form_state->set('hostgroups',$hostgroups);
		$result=\Drupal::formBuilder()->buildForm('\Drupal\zabbixfrontend\Form\zabbixfrontendHostGroupsForm', $form_state);

		return $result;

	}
	public function zabbixHosts() {
		$api=new zabbix_api();
		$form_state = new FormState();
		$hosts=$api->hostGet(array(
        'output' => 'extend',
        'search' => array()
		));
		$form_state->set('hosts',$hosts);
		$result=\Drupal::formBuilder()->buildForm('\Drupal\zabbixfrontend\Form\zabbixfrontendHostsForm', $form_state);
		return $result;	
	}
	
	public function settings() {
		return [
			'#markup' => '<h2>Página de gestión de zabbix</h2>',
		];
	}
	
}
